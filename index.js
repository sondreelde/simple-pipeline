const express = require('express')

var counter = 0

var port = process.env.PORT || 3000

var app = express()

app.get("*", (req, res) => {
    if(++counter == 69) res.send("<h1>Nice</h1>")
    else res.send(`<h1>Site has been visited ${counter} times!</h1><p>Dis newt.</p>`)
})

app.listen(port, () => 
    console.log(`Listening on port ${port}`)
)
